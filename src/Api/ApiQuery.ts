import { QueryKey, useQuery, UseQueryOptions } from 'react-query';
import { ApiParams } from "../Api/api";

export const useApiQuery = <T>(
    queryKey: QueryKey,
    params: ApiParams,
    fetchData: (params: ApiParams) => Promise<T> | T,
    options?: UseQueryOptions<T>
) => {
    return useQuery<T>(queryKey, () => fetchData(params), options);
};
