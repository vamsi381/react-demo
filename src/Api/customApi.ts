import { ApiParams,getAPI } from "./api";

export const fetchNotesDetails = async (
    params: ApiParams
): Promise<any> => {
    return await getAPI<any, void>(params, 'GET');
};

export const fetchTagsDetails = async (
    params: ApiParams
): Promise<any> => {
    return await getAPI<any, void>(params, 'GET');
};

// Save Tags Details
export const saveTagInfo = async (
    params: ApiParams
  ): Promise<any> => {
    return await getAPI<any, void>(params, 'PUT', params.data);
  };