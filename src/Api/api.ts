import axios, { Method } from 'axios';

export interface ApiParams {
    url: string;
    [key: string]: any;
}


export const getAPI = async <TRESPONSE, TDATA>(
    params: ApiParams,
    method: Method,
    data?: TDATA
) => {

    const URL = "http://localhost:8000"

    const instance = axios.create({
        baseURL: URL,
    });

    console.log('method',method);

    const response = await instance.request<TRESPONSE>({
        method,
        url: params.url,
        data: data,
    });

    return response.data;
};