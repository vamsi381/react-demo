import React, { useEffect,useState,useMemo } from "react";
import { Container, Row, Col, Card, Badge, Button } from "react-bootstrap";
import "./Notes.css";
import { useQuery } from "react-query";
import { fetchTagsDetails, saveTagInfo } from "../Api/customApi";
import { useApiQuery } from "../Api/ApiQuery";
import Form from "react-bootstrap/Form";

export const Tags = () => {

  const [tagName,setTagName] = useState("");
  
  const { isLoading, error, data } = useApiQuery<[]>(
    ["tags"],
    { url: "/tags" },
    fetchTagsDetails
  );

  console.log("data", data, "isLoading", isLoading);

  const onSubmitClick = (e:any) => {
    e.preventDefault();
    console.log('button clicked',tagName);

    saveTagInfo({
        url:"/tag",
        data:{
            tagName
        }
    }).then((response) => {
        console.log(response);
    })

  }

  const tagsInfo = useMemo(() => data?.map((x:any) => (
    <div className="tags-display">
    {x.tag_name}
    <a href="">Delete</a>
  </div>
  )), [data]);

  return (
    <div>
      <Container>
        <Row>
          <Card>
            <Card.Title>Tags</Card.Title>
            <div >
              <div>
                <Form className="tags-form" onSubmit={onSubmitClick}>
                  <Form.Group className="tags-form-grp"  controlId="">
                    <Form.Control type="text" onChange={(e) => setTagName(e.target.value)}  placeholder="Add Tag" />
                  </Form.Group>
                  <div className="tags-form-btn">
                  <Button variant="primary" type="submit">
                    Add Tag
                  </Button>
                  </div>
                </Form>
              </div>
            </div>

            <div className="col-md-12 col-xs-12">
              <div className="row">
                <div className="tags">
                  {tagsInfo}
                </div>
              </div>
            </div>
          </Card>
        </Row>
      </Container>
    </div>
  );
};
