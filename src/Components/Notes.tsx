import React, { useEffect } from "react";
import { Container, Row, Col, Card, Badge } from "react-bootstrap";
import "./Notes.css";
import { useQuery } from "react-query";
import { fetchNotesDetails } from "../Api/customApi";
import { useApiQuery } from "../Api/ApiQuery";
import { INotes } from "../interfaces/notesType";

export const Notes = () => {
  const { isLoading, error, data } = useApiQuery<[]>(
    ["notes"],
    { url: "/notes" },
    fetchNotesDetails
  );

  //console.log("data", data, "isLoading", isLoading);

  return (
    <div>
      <Container>
        <Row>
          <Card>
            <div className="col-md-12 col-xs-12">
              <div className="row">
                {
                 data &&
                  data.map((x: INotes) => (
                    <div>
                      <div className="item-info col-xs-8">
                        <p className="item-name">{x.title}</p>
                        <div className="content">
                          <div>{x.description}</div>
                        </div>
                      </div>
                      <div className="notes-tags">
                        <Badge pill bg="dark">
                          Dark
                        </Badge>
                        <Badge pill bg="dark">
                          Dark
                        </Badge>
                      </div>
                      <div style={{clear:"both"}} />
                      <hr className="notes-break" style={{ height: 3 }} />
                    </div>
                  ))}
              </div>
            </div>
          </Card>
        </Row>
      </Container>
    </div>
  );
};
