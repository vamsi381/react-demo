import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Tags } from "../Components/Tags";
import { Main } from "../Pages/Main";

export const DefaultLayout = () => (
  <div>
    <Container>
      <Row>
        <Col>Demo</Col>
      </Row>
      <Row>
        <Col xs sm={6}>
          <Main />
        </Col>
      </Row>
      <div style={{ marginTop: "50px" }} />
      <Row>
        <Col xs sm={6}>
          <Tags />
        </Col>
      </Row>
      <div style={{ marginTop: "10px" }} />
    </Container>
  </div>
);
