export interface INotes {
    title:string;
    description:string;
    created_at: Date;
}